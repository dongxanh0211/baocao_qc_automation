<?php

namespace Page;


class TaxRatesPage
{
    public static $ulrTaxRatesPage = "/administrator/index.php?option=com_redshop&view=tax_rates";

    public static $txtTaxRatesTitle = "VAT Rates";

    public static $txtTaxRatesTitleEdit = "Tax Rate [ Edit ]";

    public static $txtTaxGroupTitle = "VAT / Tax Group Management";

    public static $taxRateName = "//input[@id='jform_name']";

    public static $txtEditTaxRatesTitle = 'VAT/Tax Group [ Edit ]';

    public static $taxGroup = "//div[@id='s2id_jform_tax_group_id']//a[@class='select2-choice']";

    public static $taxGroupOption = "(//div[@class='select2-result-label'])[2]";

    public static $shopperGroup= "//ul[@class='select2-choices']";

    public static $shopperGroupOption= "//div[@id='select2-drop']//li[2]";
}