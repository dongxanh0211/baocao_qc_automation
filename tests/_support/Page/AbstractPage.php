<?php

namespace Page;


class AbstractPage
{
    public static $ulrAdmin = "/administrator";

    public static $ulrRedShopAdmin = "/administrator/index.php?option=com_redshop";

    public static $userNameField = "#mod-login-username";

    public static $passwordField = "#mod-login-password";

    public static $btnLogin = ".login-button";

    public static $txtControlPanel = "Control Panel";

    public static $txtWelcome = "Welcome";

    public static $successMessage = "Item saved.";

    public static $filterSearch = "#filter_search";

    public static $btnReset = ".reset";

    public static $alertDanger = ".alert-danger";

    public static $alertSuccess = ".alert-success";

    public static $alertNoItems = ".alert-no-items";

    public static $btnNew = ".button-new";

    public static $btnDelete = ".button-delete";

    public static $btnEdit = ".button-edit";

    public static $iconEdit = ".fa-edit";

    public static $btnSave = ".button-apply";

    public static $btnSave_Close = ".button-save";

    public static $btnSave_New = ".button-save-new";

    public static $btnCancel = ".button-cancel";

    public static $btnPublish = ".button-publish";

    public static $btnUnPublish = ".button-unpublish";

    public static $btnCheckin = ".button-checkin";

    public static $selectFirstChecbox = "#cb0";

    public static $messPublish = "successfully published";

    public static $messUnPublish = "successfully unpublished";

    public static $deleteSuccessMessage = "successfully";

    public static $copySuccessMessage = "Copied";

    public static $checkinSuccessMessage = "1 items successfully checked in.";

    public static $noticeMessage = "Please first make a selection from the list.";


}