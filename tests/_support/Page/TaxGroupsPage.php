<?php

namespace Page;


class TaxGroupsPage
{
    public static $ulrTaxGroupsPage = "/administrator/index.php?option=com_redshop&view=tax_groups";

    public static $txtTaxGroupTitle = "VAT / Tax Group Management";

    public static $taxGroupName = "//input[@id='jform_name']";

    public static $txtEditTaxGroupTitle = 'VAT/Tax Group [ Edit ]';
}