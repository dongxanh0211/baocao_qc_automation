<?php


namespace Page;


class ArticlesPage
{
    public static $ulrArticlePage = "administrator/index.php?option=com_content";

    public static $articleDropdownMenu = "//a[@class='dropdown-toggle'][contains(text(),'Content')]";

    public static $articleOption = ".menu-article";

    public static $articleText = "Articles";

    public static $articleTitle = '#jform_title';

    public static $editArea = '.form-horizontal';

    public static $dropdownMenu = "#mceu_98";

    public static $toolsDropdown = "//span[contains(text(),'Tools')]";

    public static $sourceCode = "#mceu_99-text";

    public static $edit = "#mceu_102";

    public static $imageAndLinkTab = "//a[@href=\"#images\"]";

    public static $uploadIntroImage = "//div[@id='images']//div[@class='span6']//div[1]//div[2]//div[1]//div[2]//button[1]";

    public static $imageURL = "//iframe[@name='field-media-modal']";

    public static $insertImage = "//iframe[@name='field-media-modal']";

    public static $linkATextbox = "#jform_urls_urla";

    public static $linkAText = "#jform_urls_urlatext";

    public static $successMessage = "Article saved.";

    public static $deleteSuccessMessage = "trashed.";

    public static $btnTrash = ".button-trash";

    public static $list = "//div[@id='j-main-container']";

    public static $selectAllCheckbox = "//input[@name='checkall-toggle']";

    public static $articlePage = "administrator/index.php?option=com_content";

    public static $searchTextbox = "#filter_search";

    public static $searchIcon = ".icon-search";

    public static $btnOK = "//span[contains(text(),'Ok')]";

}