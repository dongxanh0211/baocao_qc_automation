<?php
 namespace Page;

class UserPage
{
    public static $menuUserDropdown = "//a[@class='dropdown-toggle'][contains(text(),'Users')]";

    public static $menuUser = ".menu-user";

    public static $userTitle = "Users";

    public static $nameInput = "#jform_name";

    public static $userNameInput = "#jform_username";

    public static $passwordInput = "#jform_password";

    public static $passwordConfirmInput = "#jform_password2";

    public static $emailInput = "#jform_email";

    public static $resetCountLb = "#jform_resetCount-lbl";

    public static $sendMailYes = "//label[@for='jform_sendEmail0']";

    public static $jsWindow = 'window.scrollTo(0,0);';

    public static $assignedUserGroupsTab = "//ul[@id=\"myTabTabs\"]/li[2]/a";

    public static $basicSettingsTab = "//ul[@id=\"myTabTabs\"]/li[3]";

    public static $publicGroups = "//input[@id=\"1group_1\"]";

    public static $backendTemplateStyleId = "#jform_params_admin_style_chzn";

    public static $hathorDefaultStyle = "//li[contains(text(),'Hathor - Default')]";

    public static $saveSuccessMessage = "User saved.";

    public static $userList = "#userList";

    public static $userNguyen = "Nguyen";

    public static $accountDetailsTab = "//a[contains(text(),'Account Details')]";

    public static $btnDelete = "#toolbar-delete";

    public static $deleteSuccessMessage = "1 user deleted.";

    public static $listUserUrl = "administrator/index.php?option=com_users&view=users";

}