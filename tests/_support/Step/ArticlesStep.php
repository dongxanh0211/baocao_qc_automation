<?php

namespace Step;

use Page\AbstractPage;
use Page\ArticlesPage;


class ArticlesStep extends \AcceptanceTester
{
    /**
     * @param $title
     * @param $content
     * @param $imageURL
     * @param $link
     * @param $linkText
     * @throws \Exception
     */
	public function createArticle($title, $content, $link, $linkText)
	{
		$I = $this;
		$I->amOnPage(ArticlesPage::$ulrArticlePage);
		$I->waitForElementVisible(ArticlesPage::$articleDropdownMenu,30);
		$I->click(ArticlesPage::$articleDropdownMenu);
		$I->waitForElementVisible(ArticlesPage::$articleOption);
		$I->click(ArticlesPage::$articleOption);
		$I->waitForText(ArticlesPage::$articleText,30);
		$I->waitForElementVisible(AbstractPage::$btnNew,30);
		$I->click(AbstractPage::$btnNew);
		$I->waitForElementVisible(ArticlesPage::$articleTitle,30);
		$I->fillField(ArticlesPage::$articleTitle,$title);
		$I->waitForElementVisible(ArticlesPage::$editArea,30);
		$I->waitForElementVisible(ArticlesPage::$toolsDropdown,30);
		$I->click(ArticlesPage::$toolsDropdown);
		$I->waitForElementVisible(ArticlesPage::$sourceCode,30);
		$I->wait(0.1);
		$I->waitForElementClickable(ArticlesPage::$sourceCode,30);
		$I->click(ArticlesPage::$sourceCode);
		$I->fillField(ArticlesPage::$edit,$content);
		$I->waitForElementVisible(ArticlesPage::$btnOK,30);
		$I->waitForElementClickable(ArticlesPage::$btnOK,30);
		$I->wait(0.1);
		$I->click(ArticlesPage::$btnOK);
		$I->wait(0.1);
		$I->click(ArticlesPage::$imageAndLinkTab);
		$I->waitForElementVisible(ArticlesPage::$linkATextbox,30);
		$I->fillField(ArticlesPage::$linkATextbox,$link);
		$I->waitForElementVisible(ArticlesPage::$linkAText);
		$I->fillField(ArticlesPage::$linkAText,$linkText);
		$I->waitForElementVisible(AbstractPage::$btnSave_Close,30);
		$I->click(AbstractPage::$btnSave_Close);
		$I->waitForText(ArticlesPage::$successMessage,30);
	}


    /**
     * @param $titleEdit
     * @throws \Exception
     */
	public function editArticle($titleEdit)
	{
		$I = $this;
		$I->amOnPage(ArticlesPage::$articlePage);
		$I->waitForElementVisible(ArticlesPage::$list,30);
		$I->scrollTo(ArticlesPage::$list);
		$I->waitForElementVisible(AbstractPage::$selectChecbox,30);
		$I->waitForElementClickable(AbstractPage::$selectChecbox,30);
		$I->wait(0.1);
		$I->click(AbstractPage::$selectChecbox);$I->wait(2);
		$I->waitForElementVisible(AbstractPage::$btnEdit,30);
		$I->click(AbstractPage::$btnEdit);
		$I->wait(0.1);
		$I->waitForElementVisible(ArticlesPage::$articleTitle,30);
		$I->fillField(ArticlesPage::$articleTitle,$titleEdit);
		$I->click(AbstractPage::$btnSave_Close);
		$I->waitForText(ArticlesPage::$successMessage,30);
	}

    /**
     * @param $titleEdit
     * @throws \Exception
     */
	public function searchAndDelete($titleEdit)
	{
		$I = $this;
		$I->amOnPage(ArticlesPage::$articlePage);
		$I->waitForElementClickable(ArticlesPage::$list,30);
		$I->scrollTo(ArticlesPage::$list);
		$I->waitForElementVisible(ArticlesPage::$searchTextbox,30);
		$I->fillField(ArticlesPage::$searchTextbox,$titleEdit);
		$I->waitForElementVisible(ArticlesPage::$searchIcon);
		$I->click(ArticlesPage::$searchIcon);
		$I->wait(0.2);
		$I->waitForElementVisible(ArticlesPage::$selectAllCheckbox,30);
		$I->waitForElementClickable(ArticlesPage::$selectAllCheckbox,30);
		$I->click(ArticlesPage::$selectAllCheckbox);
		$I->waitForElementVisible(ArticlesPage::$btnTrash);
		$I->click(ArticlesPage::$btnTrash);
		$I->waitForText(ArticlesPage::$deleteSuccessMessage,30);
	}

	/**
	 * @throws \Exception
	 */
	public function deleteArticle()
	{
		$I = $this;
		$I->amOnPage(ArticlesPage::$articlePage);
		$I->waitForElementClickable(ArticlesPage::$list,30);
		$I->scrollTo(ArticlesPage::$list);
		$I->waitForElementVisible(ArticlesPage::$selectAllCheckbox,30);
		$I->waitForElementClickable(ArticlesPage::$selectAllCheckbox,30);
		$I->click(ArticlesPage::$selectAllCheckbox);$I->wait(0.1);
		$I->waitForElementVisible(ArticlesPage::$btnTrash);
		$I->click(ArticlesPage::$btnTrash);
		$I->waitForText(ArticlesPage::$deleteSuccessMessage,30);
	}

	/**
	 * @throws \Exception
	 */
	public function deleteAllArticle()
	{
		$I = $this;
		$I->amOnPage(ArticlesPage::$articlePage);
		$I->waitForElementClickable(ArticlesPage::$list,30);
		$I->scrollTo(ArticlesPage::$list);
		$I->waitForElementVisible(ArticlesPage::$selectAllCheckbox,30);
		$I->waitForElementClickable(ArticlesPage::$selectAllCheckbox,30);
		$I->click(ArticlesPage::$selectAllCheckbox);
		$I->wait(0.1);
		$I->waitForElementVisible(ArticlesPage::$btnTrash);
		$I->click(ArticlesPage::$btnTrash);
		$I->waitForText(ArticlesPage::$deleteSuccessMessage,30);
	}
}