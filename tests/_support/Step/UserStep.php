<?php
namespace Step;

use Page\AbstractPage;
use Page\UserPage;

class UserStep extends \AcceptanceTester
{
    /**
     * @param $name
     * @param $userName
     * @param $password
     * @param $email
     * @throws \Exception
     */
	public function createUserSaveClose($name, $userName, $password, $email)
	{
		$I = $this;
		$I->waitForElementVisible(UserPage::$menuUserDropdown);
		$I->click(UserPage::$menuUserDropdown);
		$I->waitForElementVisible(UserPage::$menuUser);
		$I->click(UserPage::$menuUser);
		$I->waitForText(UserPage::$userTitle,30);
		$I->see(UserPage::$userTitle);
		$I->click(AbstractPage::$btnNew);
		$I->waitForText(UserPage::$userTitle,30);
		$I->see(UserPage::$userTitle);
		$I->fillField(UserPage::$nameInput,$name);
		$I->fillField(UserPage::$userNameInput,$userName);
		$I->fillField(UserPage::$passwordInput,$password);
		$I->fillField(UserPage::$passwordConfirmInput,$password);
		$I->fillField(UserPage::$emailInput,$email);
		$I->scrollTo(UserPage::$resetCountLb);
		$I->waitForElementVisible(UserPage::$sendMailYes,30);
		$I->click(UserPage::$sendMailYes);
		$I->executeJS(UserPage::$jsWindow);
		$I->waitForElementVisible(UserPage::$assignedUserGroupsTab,30);
		$I->click(UserPage::$assignedUserGroupsTab);
		$I->waitForElementVisible(UserPage::$publicGroups,30);
		$I->click(UserPage::$publicGroups);
		$I->waitForElementVisible(UserPage::$basicSettingsTab,30);
		$I->click(UserPage::$basicSettingsTab);
		$I->waitForElementVisible(UserPage::$backendTemplateStyleId,30);
		$I->click(UserPage::$backendTemplateStyleId);
		$I->waitForElementVisible(UserPage::$hathorDefaultStyle,30);
		$I->click(UserPage::$hathorDefaultStyle);
		$I->waitForElementVisible(AbstractPage::$btnSave_Close,30);
		$I->click(AbstractPage::$btnSave_Close);
		$I->waitForText(UserPage::$saveSuccessMessage,30);
		$I->see(UserPage::$saveSuccessMessage);
	}

    /**
     * @param $name
     * @param $nameEdit
     * @throws \Exception
     */
	public function editUser($name, $nameEdit){
        $I = $this;
        $I->amOnPage(UserPage::$listUserUrl);
	    $I->waitForElementVisible(UserPage::$userList);
        $I->searchForItem($name);
        $I->waitForElementVisible(AbstractPage::$selectChecbox);
        $I->wait(0.5);
        $I->click(AbstractPage::$selectChecbox);
        $I->waitForElementVisible(AbstractPage::$btnEdit);
        $I->waitForElementClickable(AbstractPage::$btnEdit);
        $I->click(AbstractPage::$btnEdit);
        $I->wait(0.4);
        $I->waitForElementClickable(UserPage::$accountDetailsTab);
        $I->click(UserPage::$accountDetailsTab);
        $I->waitForElementVisible(UserPage::$nameInput);
        $I->fillField(UserPage::$nameInput, $nameEdit);
        $I->click(AbstractPage::$btnSave_Close);
        $I->waitForText(UserPage::$saveSuccessMessage,30);
    }

    /**
     * @param $nameEdit
     * @throws \Exception
     */
    public function deleteUser($nameEdit){
        $I = $this;
        $I->amOnPage(UserPage::$listUserUrl);
        $I->searchForItem($nameEdit);
        $I->waitForElementVisible(AbstractPage::$selectChecbox);
        $I->wait(0.5);
        $I->click(AbstractPage::$selectChecbox);
        $I->waitForElementVisible(AbstractPage::$selectChecbox);
        $I->waitForElementClickable(AbstractPage::$selectChecbox);
        $I->wait(0.2);
        $I->click(UserPage::$btnDelete);
        $I->acceptPopup();
        $I->waitForText(UserPage::$deleteSuccessMessage, 30);
    }
}