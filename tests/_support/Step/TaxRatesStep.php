<?php

namespace Step;

use Page\AbstractPage;
use Page\TaxRatesPage;



class TaxRatesStep extends \AcceptanceTester
{
    /**
     * @param $taxRateName
     * @param $btnName
     * @throws \Exception
     */
    public function createTaxRates($taxRateName, $btnName)
    {
        $A= new AbstractStep($this->scenario);
        $I = $this;
        $I->amOnPage(TaxRatesPage::$ulrTaxRatesPage);
        $I->waitForText(TaxRatesPage::$txtTaxRatesTitle,30);
        $I->waitForElementVisible(AbstractPage::$btnNew,30);
        $I->click(AbstractPage::$btnNew);
        $I->waitForElementVisible(TaxRatesPage::$taxRateName,30);
        $I->click(TaxRatesPage::$taxRateName);
        $I->fillField(TaxRatesPage::$taxRateName,$taxRateName);
        $I->waitForElementClickable(TaxRatesPage::$taxGroup,30);
        $I->click(TaxRatesPage::$taxGroup);
        $I->click(TaxRatesPage::$taxGroupOption);
        $I->scrollTo(TaxRatesPage::$shopperGroup);
        $I->waitForElementClickable(TaxRatesPage::$shopperGroup,30);
        $I->click(TaxRatesPage::$shopperGroup);
        $I->click(TaxRatesPage::$shopperGroupOption);
        $A->clickButton($btnName,TaxRatesPage::$txtTaxRatesTitle);

    }

    /**
     * @param $taxRateName
     * @param $txtTaxRatesEdit
     * @param $btnName
     * @throws \Exception
     */
    public function editTaxRates($taxRateName,$txtTaxRatesEdit, $btnName)
    {
        $A= new AbstractStep($this->scenario);
        $I = $this;
        $I->amOnPage(TaxRatesPage::$ulrTaxRatesPage);
        $A->search(AbstractPage::$filterSearch,$taxRateName);
        $I->waitForElementVisible(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->canSee($taxRateName);
        $I->waitForElementVisible(AbstractPage::$iconEdit,30);
        $I->click(AbstractPage::$iconEdit);
        $I->canSee(TaxRatesPage::$txtTaxRatesTitleEdit);
        $I->fillField(TaxRatesPage::$taxRateName,$txtTaxRatesEdit);
        $I->wait(0.5);
        $A->clickButton($btnName,TaxRatesPage::$txtTaxRatesTitle);
    }
}