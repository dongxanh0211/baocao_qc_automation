<?php

namespace Step;


use Page\AbstractPage;
use Page\TaxGroupsPage;

class AbstractStep extends \AcceptanceTester
{
    /**
     * @param $userName
     * @param $passWord
     * @throws \Exception
     */
    public function login($userName, $passWord)
    {
        $I = $this;
        $I->amOnPage(AbstractPage::$ulrAdmin);
        $I->waitForElementVisible(AbstractPage::$userNameField, 30);
        $I->fillField(AbstractPage::$userNameField, $userName);
        $I->fillField(AbstractPage::$passwordField, $passWord);
        $I->waitForElementVisible(AbstractPage::$btnLogin, 30);
        $I->click(AbstractPage::$btnLogin);
        $I->waitForText(AbstractPage::$txtControlPanel, 30);
        $I->see(AbstractPage::$txtControlPanel);
    }

    /**
     * @param $userName
     * @param $passWord
     * @throws \Exception
     */
    public function loginRedShop($userName, $passWord)
    {
        $I = $this;
        $I->amOnPage(AbstractPage::$ulrRedShopAdmin);
        $I->waitForElementVisible(AbstractPage::$userNameField, 30);
        $I->fillField(AbstractPage::$userNameField, $userName);
        $I->fillField(AbstractPage::$passwordField, $passWord);
        $I->waitForElementVisible(AbstractPage::$btnLogin, 30);
        $I->click(AbstractPage::$btnLogin);
        $I->waitForText(AbstractPage::$txtWelcome, 30);
        $I->see(AbstractPage::$txtWelcome);
    }

    /**
     * @param $taxGroupName
     * @throws \Exception
     */
    public function searchTaxGroup($taxGroupName)
    {
        $I = $this;
        $I->amOnPage(TaxGroupsPage::$ulrTaxGroupsPage);
        $I->waitForText(TaxGroupsPage::$txtTaxGroupTitle,30);
        $I->scrollTo(AbstractPage::$filterSearch);
        $I->click(AbstractPage::$filterSearch);
        $I->fillField(AbstractPage::$filterSearch, $taxGroupName);
        $I->wait(0.5);
        $I->pressKey(AbstractPage::$filterSearch,\Facebook\WebDriver\WebDriverKeys::ENTER);
        $I->wait(3);
        $I->canSee($taxGroupName);
    }

    /**
     * @param $filterSearch
     * @param $text
     * @throws \Exception
     */
    public function search($filterSearch, $text)
    {
        $I = $this;
        $I->waitForElementVisible(AbstractPage::$btnReset);
        $I->waitForElementClickable(AbstractPage::$btnReset);
        $I->wait(0.2);
        $I->click(AbstractPage::$btnReset);
        $I->waitForElementVisible($filterSearch,30);
        $I->click($filterSearch);
        $I->fillField($filterSearch,$text);
        $I->pressKey($filterSearch,\Facebook\WebDriver\WebDriverKeys::ENTER);
    }


    public function clickButton($button,$title)
    {
        $I = $this;
        switch($button){
            case AbstractPage::$btnSave:
                {
                    $I->click(AbstractPage::$btnSave);
                    $I->see($title);
                    break;
                }
            case AbstractPage::$btnSave_Close:
                {
                    $I->click(AbstractPage::$btnSave_Close);
                    $I->see($title);
                    break;
                }
            case AbstractPage::$btnSave_New:
                {
                    $I->click(AbstractPage::$btnSave_New);
                    $I->see($title);
                    break;
                }
            case AbstractPage::$btnCancel:
                {
                    $I->click(AbstractPage::$btnCancel);
                    $I->see($title);
                    break;
                }
        }
    }

    /**
     * @param $url
     * @param $item
     * @throws \Exception
     */
        public function publish($url, $item)
    {
        $I = $this;
        $A = new AbstractStep($this->scenario);
        $I->amOnPage($url);
        $A->search(AbstractPage::$filterSearch,$item);
        $I->waitForElementVisible(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$btnPublish);
        $I->waitForText(AbstractPage::$messPublish,30);
    }

    /**
     * @param $url
     * @param $item
     * @throws \Exception
     */
    public function unPublish($url, $item)
    {
        $I = $this;
        $A = new AbstractStep($this->scenario);
        $I->amOnPage($url);
        $A->search(AbstractPage::$filterSearch,$item);
        $I->waitForElementVisible(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$btnUnPublish);
        $I->waitForText(AbstractPage::$messUnPublish,30);
    }

    /**
     * @param $url
     * @param $searchField
     * @param $item
     * @throws \Exception
     */
    public function checkin1($url, $searchField, $item)
    {
        $I = $this;
        $A = new AbstractStep($this->scenario);
        $I->amOnPage($url);
        $A->search($searchField, $item);
        $I->waitForElementVisible(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$btnCheckin);
        $I->waitForText(AbstractPage::$alertSuccess,30);
    }

    /**
     * @param $url
     * @param $button
     * @param $title
     * @param $message
     * @throws \Exception
     */
    public function checkin($url,$button,$title,$message){
        $I = $this;
        $I->amOnPage($url);
        $I->search($button,$title);
        $I->waitForElementClickable(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->waitForElementVisible(AbstractPage::$btnCheckin);
        $I->click(AbstractPage::$btnCheckin);
        $I->waitForText($message, 30);

    }

    /**
     * @param $url
     * @param $button
     * @param $title
     * @param $message
     * @throws \Exception
     */
    public function delete($url,$button,$title,$message){
        $I = $this;
        $I->amOnPage($url);
        $I->search($button,$title);
        $I->waitForElementClickable(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->waitForElementVisible(AbstractPage::$btnDelete);
        $I->click(AbstractPage::$btnDelete);
        $I->acceptPopup();
        $I->waitForText($message, 30);
    }

    /**
     * @param $url
     * @param $searchField
     * @param $item
     * @param $button
     * @throws \Exception
     */
    public function nonChecked($url, $searchField, $item,$button)
    {
        $I = $this;
        $A = new AbstractStep($this->scenario);
        $I->amOnPage($url);
        $A->search($searchField, $item);
        $I->click($button);
        $I->seeInPopup(AbstractPage::$noticeMessage);
        $I->acceptPopup();
    }

}