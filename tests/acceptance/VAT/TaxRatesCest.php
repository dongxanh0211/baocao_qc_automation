<?php

use Step\AbstractStep;
use Page\AbstractPage;
use Page\TaxRatesPage;
use Step\TaxRatesStep;



class TaxRatesCest
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $userNameAdmin;

    /**
     * @var string
     */
    protected $passWordAdmin;

    /**
     * @var string
     */
    protected $taxRateName;

    /**
     * @var string
     */
    protected $taxRateNameEdit;
    /**
     * @var
     */
    protected $btnName;

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
        $this->userNameAdmin = "admin";
        $this->passWordAdmin = "admin";
        $this->taxRateName = $this->faker->lexify('Rates name ?????');
        $this->taxRateNameEdit = $this->faker->lexify('Rates name edit ?????');
    }

    /**
     * @param TaxRatesStep $I
     * @param AbstractStep $step
     * @throws Exception
     */
    public function createTaxRates (TaxRatesStep $I, AbstractStep $step)
    {
        $step->loginRedShop($this->userNameAdmin, $this->passWordAdmin);
        $I->createTaxRates($this->taxRateName,AbstractPage::$btnSave_Close);
        //$I->editTaxRates($this->taxRateName,$this->taxRateNameEdit, AbstractPage::$btnSave_Close);
        $step->checkin(TaxRatesPage::$ulrTaxRatesPage,AbstractPage::$filterSearch,$this->taxRateName,AbstractPage::$checkinSuccessMessage);
        $step->nonChecked(TaxRatesPage::$ulrTaxRatesPage,AbstractPage::$filterSearch,$this->taxRateName,AbstractPage::$btnCheckin);
        $step->delete(TaxRatesPage::$ulrTaxRatesPage,AbstractPage::$filterSearch,$this->taxRateName,AbstractPage::$deleteSuccessMessage);

    }

}