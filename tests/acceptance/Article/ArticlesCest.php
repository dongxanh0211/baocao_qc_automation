<?php


use Step\AbstractStep;
use Step\ArticlesStep;

class ArticlesCest
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $userNameAdmin;

    /**
     * @var string
     */
    protected $passWordAdmin;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $link;

    /**
     * @var string
     */
    protected $linkText;

    /**
     * @var string
     */
    protected $titleEdit;

    /**
     * @var string
     */
    protected $uploadIntroImage;

    /**
     * @var string
     */
    protected $imageURL;


    public function __construct()
    {
        $this->faker = Faker\Factory::create();
        $this->userNameAdmin = "admin";
        $this->passWordAdmin = "admin";
        $this->title = $this->faker->lexify('Title ????');;
        $this->content = $this->faker->text($maxNbChars = 200);
        $this->link = $this->faker->url;
        $this->uploadIntroImage = $this->faker->image();
        $this->imageURL = $this->faker->imageUrl();
        $this->linkText = $this->faker->bothify('This is linkText ##??');
        $this->titleEdit=$this->faker->lexify('Edit ????');
    }

    /**
     * @param ArticlesStep $I
     * @param AbstractStep $step
     * @throws Exception
     */
    public function createEditDeleteArticle(ArticlesStep $I, AbstractStep $step)
    {

        $step->login($this->userNameAdmin, $this->passWordAdmin);
        $I->createArticle($this->title, $this->content, $this->link, $this->linkText);
        $I->editArticle($this->titleEdit);
        $I->searchAndDelete($this->titleEdit);
    }
}