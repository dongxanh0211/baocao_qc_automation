<?php


use Step\AbstractStep;
use Step\UserStep;

class UserCest
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $userNameAdmin;

    /**
     * @var string
     */
    protected $passWordAdmin;

    /**
     * @var string
     */
    protected $nameUser;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var string
     */
    protected $passWord;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $nameEdit;

    /**
     * UserCest constructor.
     */



    public function __construct()
    {
        $this->faker = Faker\Factory::create();
        $this->userNameAdmin = "admin";
        $this->passWordAdmin = "admin";
        $this->nameUser = $this->faker->name;
        $this->nameUser = $this->faker->bothify("Name user ??##");
        $this->userName = $this->faker->userName;
        $this->passWord = $this->faker->password(5,8);
        $this->email = $this->faker->email;
        $this->nameEdit = $this->faker->bothify("Name edit ??##");

        $this->userName = array();
    }



    /**
     * @param UserStep $I
     * @param AbstractStep $step
     * @throws Exception
     */
    public function createEditDeleteUser(UserStep $I, AbstractStep $step)
    {
        $step->login($this->userNameAdmin, $this->passWordAdmin);

        $I->createUserSaveClose($this->nameUser, $this->userName, $this->passWord, $this->email);

        $I->editUser($this->nameUser, $this->nameEdit);

        $I->deleteUser($this->nameEdit);
    }
}