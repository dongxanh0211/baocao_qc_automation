<?php 

class FirstCest
{
    public function login(\AcceptanceTester $I)
    {
        $I->amOnPage("/administrator");
        $I->waitForElementVisible("#mod-login-username", 30);
        $I->fillField("#mod-login-username", "admin");
        $I->fillField("#mod-login-password", "admin");
        $I->waitForElementVisible(".login-button", 30);
        $I->click(".login-button");
        $I->waitForText("Control Panel", 30);
        $I->see("Control Panel");
    }


}
