# Mở đầu

Đây là repository phục vụ cho việc dạy học khóa QC Automation 06/2020 được tổ chức bởi: CrossTech

## Yêu cầu với mentees

- Các bạn tạo mỗi người 1 folder riêng. Ghi họ tên của mình bằng **chữ thường, không dấu, thay khoảng trắng bằng dấu _**
- Sau đó các bạn tách riêng cho mình mỗi người 1 branch
- Mọi commit của các bạn sẽ được push lên branch mà mình đã tạo

## Một số lưu ý khi dùng git

- Nên git pull branch master. Sau đó merge master vào branch của mình, rồi mới commit và push code
- Các bạn có thể tham khảo code của nhau bằng cách switch sang branch của bạn khác. Nhưng **tuyệt đối** không được sửa code của người khác và commit lên

## Hướng dẫn sử dụng git đơn giản tại https://rogerdudler.github.io/git-guide/index.vi.html

## Timeline 20/06/2020 - 22/08/2020
| # | Nội dung |
| -------      |:---------|
| 1   |  Overview Automation testing |
| 2  | Why is Test Automation?|
| 3  |      Automation Based on the type of testing |
| 4  |    Introduction Selenium |
| 5  |     Codeception |
| 6  |     Setup project Codeception |
| 7 |     Workflow of Automation Test |
| 8 |     CI/CD |
| 9  |     Final Report |

